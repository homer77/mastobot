# README
## About mastobot
This simple python3 script's purpose is just to copy a number of twitter accounts' statuses to corresponding Mastodon accounts.
## Prerequisites
To use #mastobot you need to install tweepy, Mastodon.py and wget. To do so you could e.g. just use pip3 by entering commandline:
``` sudo pip3 install tweepy ```
and vice versa.
Also, you have of course to register the goal accounts on Mastodon and to adjust both the mastodon.py file's _setting_path_ pointing to the absolute path to the settings.json file and
the setting.json file itself regarding to your needs.
Above this you need a twitter developer account to receive consumer and access keypairs. For a HowTo please take a look at the tweepy documentation.

## Usage
After having filled in every information into the settings.json file you could use e.g. ```crontab -e``` on unixoid systems from shell and add a line like
```*/15 * * * *   python3 /home/username/mastobot/mastobot.py >> /home/username/mastobot.log``` which lets the script be triggered every 15 minutes.
By default the script will be limited to 25 tweets per account for this period which should definitely be enough for a human account I think.

## Warning
Before you start flooding your Mastodon instance accidently with hundreds of repeating posts, please test your adjustments by hitting
```python3 mastodon.py``` and have a look on the output. If the script breaks at any time the 'last_id'-parameters won't be updated and next time it will post exactly the same statuses again and again!
So make sure that the script runs successfully at least once before you fire this gun!

## Unlock script
By default the script has commented out the commands to upload anything to your Mastodon instance (see 'Warning' above).
To unlock it just uncomment
```
# masto_photo = mastodon.media_post(tmp_path)               
# masto_media.append(masto_photo)
...
# mastodon.status_post(mastodon_text, None, masto_media, True)
```
Please again start the script once by hand from commandline and _then_ start your cronjob if you wish to.